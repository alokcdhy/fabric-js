let mousePressed = false;
let currentMode = "";
const root = `${window.location.origin}/`;
const api_root = ``;
// initialize canvas
const initCanvas = (id) => {
  return new fabric.Canvas(id, {
    width: 500,
    height: 500,
    selection: false,
  });
};

// set background image
const setBackground = (url) => {
  fabric.Image.fromURL(url, (img) => {
    canvas.backgroundImage = img;
    canvas.renderAll();
  });
};

// add image to canvas
const addImage = () => {
  let url =
    "https://www.veryicon.com/icon/png/Flag/Rounded%20World%20Flags/Nepal%20Flag.png";
  fabric.Image.fromURL(url, (img) => {
    canvas.add(img);
    canvas.renderAll();
  });
};

// add text field to canvas
const addText = () => {
  let t1 = new fabric.Textbox("My Text", {
    width: 200,
    top: 5,
    left: 5,
    fontSize: 16,
    textAlign: "center",
  });

  let t2 = new fabric.Textbox(
    "My text is longer, but I do not want the box to grow, or the text to wrap. I only want the text to fit the available size",
    {
      width: 200,
      height: 200,
      top: 250,
      left: 5,
      fontSize: 16,
      textAlign: "center",
    }
  );
  canvas.add(t1);
  canvas.add(t2);
};

// mouse press modes
const modes = {
  pan: "pan",
  drawing: "drawing",
};

// mouse press modes event
const toggleMode = (mode) => {
  if (mode === modes.pan) {
    currentMode = currentMode === modes.pan ? "" : modes.pan;
  } else if (mode === modes.drawing) {
    if (currentMode === modes.drawing) {
      currentMode = "";
      canvas.isDrawingMode = false;
      canvas.renderAll();
    } else {
      currentMode = modes.drawing;
    }
  }

  if (currentMode == modes.drawing) {
    $("#drawing-mode-options").show();
  } else {
    $("#drawing-mode-options").hide();
  }
};

// mouse events i.e operation for pan modes
const setMouseEents = () => {
  canvas.on("mouse:move", (event) => {
    if (mousePressed && currentMode === modes.pan) {
      canvas.setCursor("grab");
      canvas.renderAll();
      const mEvent = event.e;
      const delta = new fabric.Point(mEvent.movementX, mEvent.movementY);
      canvas.relativePan(delta);
    } else if (mousePressed && currentMode === modes.drawing) {
      canvas.selection = true;
      canvas.isDrawingMode = true;
      canvas.renderAll();
    }
  });

  canvas.on("mouse:down", (event) => {
    mousePressed = true;
    if (currentMode === modes.pan) {
      canvas.setCursor("grab");
      canvas.renderAll();
    }
  });

  canvas.on("mouse:up", (event) => {
    mousePressed = false;
    canvas.setCursor("default");
    canvas.renderAll();
  });
};

// clear all objects from canvas
const clearCanvas = () => {
  canvas.getObjects().forEach((obj, index) => {
    if (obj != canvas.backgroundImage) {
      console.log("Remove object : ", obj);
      canvas.remove(obj);
    }
  });
};

// drop selected oject partical
const dropCanvasObject = () => {
  const activeObj = canvas.getActiveObject();
  if (canvas.getObjects().indexOf(activeObj) >= 0) {
    console.log(
      "Remove object : ",
      canvas.getObjects().indexOf(activeObj),
      canvas.getObjects()[canvas.getObjects().indexOf(activeObj)]
    );
    canvas.remove(
      canvas.getObjects().indexOf(activeObj),
      canvas.getObjects()[canvas.getObjects().indexOf(activeObj)]
    );
    saveCanvas(canvas);
  } else {
    console.log("No object selected.");
  }
};

// get svg from canvas
const saveCanvas = () => {
  let svg = canvas.toSVG();
  console.log(svg);
  // set data as json format
  // localStorage.setItem('canvasObjects',JSON.stringify(canvas));
  // console.log('saved canvas data : ',JSON.parse(localStorage.getItem('canvasObjects')));
};

// loade svg as canvas
const canvasLoad = () => {
  let svgURL = `${root}assets/img/nepal.svg`;
  console.log(svgURL);
  fabric.loadSVGFromURL(svgURL, function (objects, options) {
    var obj = fabric.util.groupSVGElements(objects, options);
    canvas.add(obj).renderAll();
  });

  // load canvas data from json format
  // if (localStorage.getItem('canvasObjects')){
  //     console.log("get canvas data : ",JSON.parse(localStorage.getItem('canvasObjects')));
  //     let canvasObject = JSON.parse(localStorage.getItem('canvasObjects'));
  //     canvas.loadFromJSON(canvasObject);
  //     canvas.renderAll.bind(canvasObject);
  // }
};

// default access
let canvas = initCanvas("canvas");
setMouseEents();
setBackground(
  "https://www.securityroundtable.org/wp-content/uploads/2018/07/AdobeStock_103287945.jpeg"
);
